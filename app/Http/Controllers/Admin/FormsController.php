<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Form;
use Illuminate\Http\Request;

class FormsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $forms = Form::where('firstname', 'LIKE', "%$keyword%")
                ->orWhere('lastname', 'LIKE', "%$keyword%")
                ->orWhere('nickname', 'LIKE', "%$keyword%")
                ->orWhere('idcard', 'LIKE', "%$keyword%")
                ->orWhere('birthdate', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('address', 'LIKE', "%$keyword%")
                ->orWhere('backlist', 'LIKE', "%$keyword%")
                ->orWhere('note', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $forms = Form::latest()->paginate($perPage);
        }

        return view('admin.forms.index', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.forms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'firstname' => 'required',
			'lastname' => 'required',
			'nickname' => 'required',
			'idcard' => 'required|max:13',
			'birthdate' => 'required',
			'phone' => 'required|max:10',
			'address' => 'required',
			'backlist' => 'required',
			'note' => 'required'
		]);
        $requestData = $request->all();
        
        Form::create($requestData);

        return redirect('admin/forms')->with('flash_message', 'Form added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $form = Form::findOrFail($id);

        return view('admin.forms.show', compact('form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $form = Form::findOrFail($id);

        return view('admin.forms.edit', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'firstname' => 'required',
			'lastname' => 'required',
			'nickname' => 'required',
			'idcard' => 'required|max:13',
			'birthdate' => 'required',
			'phone' => 'required|max:10',
			'address' => 'required',
			'backlist' => 'required',
			'note' => 'required'
		]);
        $requestData = $request->all();
        
        $form = Form::findOrFail($id);
        $form->update($requestData);

        return redirect('admin/forms')->with('flash_message', 'Form updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Form::destroy($id);

        return redirect('admin/forms')->with('flash_message', 'Form deleted!');
    }
}
