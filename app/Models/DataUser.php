<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'data_users';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'nickname', 'idcard', 'birthdate', 'phone', 'address', 'backlist', 'note'];

    
}
