
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Items</div>
                    <div class="card-body">
                            <a href="{{ url('/admin/forms/create') }}" class="btn btn-success btn-sm" title="Add New Form">
                                    <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มลูกค้าใหม่
                            </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </thead>
                                <tbody>
                                @foreach($user as $users) 
                                    <tr>
                                        <td>{{ $users->id }}</td>
                                        <td>{{ $users->name }}</td>
                                        <td>{{ $users->role }}</td>
                                        <td>
                                            <a href="{{ url('/admin/adduser/' . $users->id . '/edit') }}" title="Edit Form"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</button></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection
