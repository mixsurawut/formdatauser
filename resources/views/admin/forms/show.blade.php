@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Form {{ $form->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/forms') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @if(Auth::user()->role ==2 || Auth::user()->role == 3)
                        <a href="{{ url('/admin/forms/' . $form->id . '/edit') }}" title="Edit Form"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/forms' . '/' . $form->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Form" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $form->id }}</td>
                                    </tr>
                                    <tr>
                                    <th> Firstname </th>
                                    <td> {{ $form->firstname }} </td>
                                    </tr>
                                    <tr>
                                    <th> Lastname </th>
                                    <td> {{ $form->lastname }} </td>
                                    </tr>
                                    <tr>
                                    <th> Nickname </th>
                                    <td> {{ $form->nickname }} </td>
                                    </tr>
                                    <tr>
                                    <th> Backlist </th>
                                    <td> {{ $form->backlist }} </td>
                                    </tr>
                                    <tr>
                                    <th> Idcard </th>
                                    <td> {{ $form->idcard }} </td>
                                    </tr>
                                    <tr>
                                    <th> Birthdate </th>
                                    <td> {{ $form->birthdate }} </td>
                                    </tr>
                                    <tr>
                                    <th> Phone </th>
                                    <td> {{ $form->phone }} </td>
                                    </tr>
                                    <tr>
                                    <th> Address </th>
                                    <td> {{ $form->address }} </td>
                                    </tr>
                                    <tr>
                                    <th> Note </th>
                                    <td> {{ $form->note }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
