<div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    <label for="firstname" class="control-label">{{ 'ชื่อจริง' }}</label>
    <input class="form-control" name="firstname" type="text" id="firstname" value="{{ isset($form->firstname) ? $form->firstname : ''}}" required>
    {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
    <label for="lastname" class="control-label">{{ 'นามสกุล' }}</label>
    <input class="form-control" name="lastname" type="text" id="lastname" value="{{ isset($form->lastname) ? $form->lastname : ''}}" required>
    {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nickname') ? 'has-error' : ''}}">
    <label for="nickname" class="control-label">{{ 'ชื่อเล่น' }}</label>
    <input class="form-control" name="nickname" type="text" id="nickname" value="{{ isset($form->nickname) ? $form->nickname : ''}}" required>
    {!! $errors->first('nickname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('idcard') ? 'has-error' : ''}}">
    <label for="idcard" class="control-label">{{ 'เลขบัตรประชาชน' }}</label>
    <input class="form-control" name="idcard" type="text" id="idcard" value="{{ isset($form->idcard) ? $form->idcard : ''}}" required>
    {!! $errors->first('idcard', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('birthdate') ? 'has-error' : ''}}">
    <label for="birthdate" class="control-label">{{ 'วันเกิด (วัน/เดือน/ปี) (เวลาที่กรอกข้อมูล)' }}</label>
    <input class="form-control" name="birthdate" type="datetime-local" id="birthdate" value="{{ isset($form->birthdate) ? $form->birthdate : ''}}" required>
    {!! $errors->first('birthdate', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'เบอร์โทรศัพท์' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($form->phone) ? $form->phone : ''}}" required>
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'ที่อยู่' }}</label>
    <textarea class="form-control" rows="5" name="address" type="textarea" id="address" required>{{ isset($form->address) ? $form->address : ''}}</textarea>
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('backlist') ? 'has-error' : ''}}">
    <label for="backlist" class="control-label">{{ 'แบล็คลิส' }}</label>
    <textarea class="form-control" rows="5" name="backlist" type="textarea" id="backlist" required>{{ isset($form->backlist) ? $form->backlist : ''}}</textarea>
    {!! $errors->first('backlist', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'บันทึกต่างๆ' }}</label>
    <textarea class="form-control" rows="5" name="note" type="textarea" id="note" required>{{ isset($form->note) ? $form->note : ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
