<div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    <label for="firstname" class="control-label">{{ 'Firstname' }}</label>
    <input class="form-control" name="firstname" type="text" id="firstname" value="{{ isset($data->firstname) ? $data->firstname : ''}}" required>
    {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
    <label for="lastname" class="control-label">{{ 'Lastname' }}</label>
    <input class="form-control" name="lastname" type="text" id="lastname" value="{{ isset($data->lastname) ? $data->lastname : ''}}" required>
    {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nickname') ? 'has-error' : ''}}">
    <label for="nickname" class="control-label">{{ 'Nickname' }}</label>
    <input class="form-control" name="nickname" type="text" id="nickname" value="{{ isset($data->nickname) ? $data->nickname : ''}}" required>
    {!! $errors->first('nickname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('idcard') ? 'has-error' : ''}}">
    <label for="idcard" class="control-label">{{ 'Idcard' }}</label>
    <input class="form-control" name="idcard" type="text" id="idcard" value="{{ isset($data->idcard) ? $data->idcard : ''}}" required>
    {!! $errors->first('idcard', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('birthdate') ? 'has-error' : ''}}">
    <label for="birthdate" class="control-label">{{ 'Birthdate' }}</label>
    <input class="form-control" name="birthdate" type="datetime-local" id="birthdate" value="{{ isset($data->birthdate) ? $data->birthdate : ''}}" required>
    {!! $errors->first('birthdate', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($data->phone) ? $data->phone : ''}}" required>
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Address' }}</label>
    <textarea class="form-control" rows="5" name="address" type="textarea" id="address" required>{{ isset($data->address) ? $data->address : ''}}</textarea>
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('backlist') ? 'has-error' : ''}}">
    <label for="backlist" class="control-label">{{ 'Backlist' }}</label>
    <textarea class="form-control" rows="5" name="backlist" type="textarea" id="backlist" required>{{ isset($data->backlist) ? $data->backlist : ''}}</textarea>
    {!! $errors->first('backlist', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('note') ? 'has-error' : ''}}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <textarea class="form-control" rows="5" name="note" type="textarea" id="note" required>{{ isset($data->note) ? $data->note : ''}}</textarea>
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
