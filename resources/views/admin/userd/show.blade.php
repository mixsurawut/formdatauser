@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Userd {{ $userd->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/userd') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @if(Auth::user()->role == 3)
                        <a href="{{ url('/admin/userd/' . $userd->id . '/edit') }}" title="Edit Userd"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                       
                        
                        <form method="POST" action="{{ url('admin/userd' . '/' . $userd->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Userd" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endif

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $userd->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $userd->name }} </td></tr><tr><th> Email </th><td> {{ $userd->email }} </td></tr><tr><th> Password </th><td> {{ $userd->password }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
