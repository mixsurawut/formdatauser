<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a class="nav-link" href="{{ url('/admin/forms') }}">
                        หน้ารายชื่อ
                    </a>
                    @if(Auth::user()->role ==2 || Auth::user()->role == 3)
                    <a class="nav-link" href="{{ url('/admin/userd') }}">
                        หน้าเพิ่มผู้ใช้งาน
                    </a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</div>
